/** 
 * our action is wrapped by a function which we refer to as an action creator (selectLibrary).
 * whenever we call this action creator, the returned action will be automatically dispactched to
 * all the reducers. 
 * action creators are functions that return actions. actions are objects that have a "type" property.
 * actions tell reducers to update part of state data in a specific fashion.
*/
export const selectLibrary = (libraryId) =>{
    return {
        /** 
         * an object with a "type" property is an action, and an action is 
         * how we get our reducers to update the data they produce.
        */
        type: 'select_library',
        payload: libraryId
    };
};
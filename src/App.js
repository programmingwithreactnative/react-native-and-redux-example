import React from 'react';
import {View} from 'react-native';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import reducers from './reducers';
import {Header} from './components/common';
import LibraryList from './components/LibraryList';
/** 
 * provider is the communication glue between redux and react
 * provider can have only a single child. so we wrap all components in a view
*/
const App = ()=>{
    return(
        <Provider store={createStore(reducers)}>
        <View>
            <Header headerText="Tech List"/>
            <LibraryList />
        </View>
        </Provider>
    );
};

export default App;
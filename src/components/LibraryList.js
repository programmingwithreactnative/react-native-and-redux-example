import React, {Component} from 'react';
import {FlatList} from 'react-native';
import {connect} from 'react-redux';
import ListItem from './ListItem';

class LibraryList extends Component{

    renderItem(library){
        return <ListItem library={library} />;
    }

    render(){
        console.log("Library List: " + this.props);
        return (
            <FlatList 
                data={this.props.libs}
                renderItem={this.renderItem}
                keyExtractor={(library)=>library.id.toString()}
            />
        );
    }
}
/**
 * take the global state object, map it in some fashion and provide 
 * them as props to our component LibraryList
 * if I return an object here, that object will show up as props to LibraryList component 
 * 
 * "connect" forges a connection between the redux side and the react side of our application
 * 
 * mapStateToProps is not a hard-coded name. It can be any name. 
 */
const mapStateToProps = state => {
    console.log(state);
    return {libs: state.libraries};
};
export default connect(mapStateToProps)(LibraryList);
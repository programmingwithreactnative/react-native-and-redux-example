import React, {Component} from 'react';
import {
    Text, 
    TouchableWithoutFeedback, 
    View, 
    LayoutAnimation,
    NativeModules
} from 'react-native';
import {CardSection} from './common';
import * as actions from '../actions';
import {connect} from 'react-redux';
/**
 * 1- user presses a library
 * 2- action creator is called
 * 3- returned action is dispacted automatically to all the reducers
 * 4- reducers re-run and they assamble some amount of new app state (store state)
 * 5- this new state is automatically sent down to mapStateToProps functions
 * 6- this causes our components to re-render
 * 7- our view updates on the screen
 * 8- LayoutAnimation is a little hook on the last step. it is called anytime before our components
 * rendered(updated) on the screen, all the updates on the screen will automatically be animated for us.
 */
//the below UIManager configuration is required to enable the animation in android!
const {UIManager} = NativeModules;
UIManager.setLayoutAnimationEnabledExperimental && 
UIManager.setLayoutAnimationEnabledExperimental(true);
/**
 * in LibraryList component we used connect helper to access application state(store state).
 * connect helper can also be used for calling an action creator.
 */
class ListItem extends Component{
    //lifecycle method. automatically called whenever a component is about to be re-rendered
    componentWillUpdate(){
        LayoutAnimation.spring();
    }
    renderDescription(){
        const {library, expanded} = this.props;
        if(expanded){
            return(
                <CardSection>
                    <Text>{library.item.description}</Text>
                </CardSection>
            );
        }
    }
    render(){
        const {titleStyle} = styles;
        const {id, title} = this.props.library.item;
        console.log("List Item: " + this.props);
        return(
            <TouchableWithoutFeedback onPress={()=>this.props.selectLibrary(id)}>
                <View>
                    <CardSection>
                        <Text style={titleStyle}>
                            {title}
                        </Text>
                    </CardSection>
                    <CardSection>
                        {this.renderDescription()}
                    </CardSection>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = {
  titleStyle:{
      fontSize: 18,
      paddingLeft: 15
  }  
};
/**
 * second argument by conventions is called ownProps (you can call it whatever you want)
 * ownProps are the props that are passed to the ListItem component.
 * ownProps is exactly equal to this.props inside the ListItem component.
 * Remember, ListItem is passed a library from the LibraryList component. 
 * so we can move all logic from the component into mapStateToProps function and do some
 * pre calculation, and pass down an appropriate prop into ListItem component.
 */
const mapStateToProps = (state, ownProps) => {
    const expanded = state.selectedLibraryId === ownProps.library.item.id;
    return {expanded:expanded};
};
/**
 * wiring action-creator functions to redux. without this wiring, they are just simple java functions
 * that return a simple java object. This way our functions are turning into action creators and 
 * when they are called the returned action will be automatically dispatched to the redux store. 
 * we do not need to explicitly call dispatched ourselves here. automatic binding with the connect 
 * helper does that for us.
 * All the action creators are passed as props to our component here.
 * they can be called like {this.props.selectLibrary}
 */
export default connect(mapStateToProps, actions)(ListItem);
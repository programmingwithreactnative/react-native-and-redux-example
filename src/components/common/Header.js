import React from 'react';
import { Text, View } from 'react-native';
//props is used for parent-child communication. for passing data from parent
//to child to dynamically set some values to render. This is a great way to
//make reusable components. avoid hard-coding values into childs.
const Header = (props) => {
    const { textStyle, viewStyle } = styles;//destructuring

    return (
      //we could have done styles.viewStyle & styles.textStyle if we had not destructured above
      //whenever we wanna reference a javascript variable within JSX we wrap it with {}
      <View style={viewStyle}>
        <Text style={textStyle}>{props.headerText}</Text>
      </View>
    );
  };

  const styles = {
    viewStyle: {
      backgroundColor: '#f5f5f5',
      justifyContent: 'center',
      alignItems: 'center',
      height: 60,
      paddingTop: 15,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.5,
      elevation: 2,
      position: 'relative'
    },
    textStyle: {
      fontSize: 20
    }
  };

//make this component available to other parts of the app
export {Header};

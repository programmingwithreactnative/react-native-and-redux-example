/**
 * state -> state that was returned the last time this reducer was executed
 * action -> dispatched action
 * defaulting state to null as initial state so that it is not undefined when applcation
 * boots up for the first time.
 */
export default (state = null, action)=>{
    /** 
     * by default we will not have a selected library.
     * return cannot be undefined otherwise redux will consider this as an error.
     * so if we don't want a default selection, we return null.
    */
   console.log(action);
    switch(action.type){
        case 'select_library':
            return action.payload;

        default:
        /**
         * if an action type this reducer does not carea about is received, we return the 
         * last state that was returned by this reducer. we cannot return an undifed value because
         * redux will treat it as an error.
         *  */
            return state;
    }
};
import {combineReducers} from 'redux';
import LibraryReducer from './LibraryReducer';
import SelectionReducer from './SelectionReducer';
/**
 * a reducer is a function that returns some amount of data
 * whenever we say data in a redux application we talk about reducers.
 * reducers create our application state 
 * and application state hold all the date for our app
 * like our state is our app's data
 * in this app we have 2 pieces of state (list of tech/libraries & selected tech)
 * so we use 2 seperate reducers
 * combineReducers is the thing that make multiple reducers work together nicely
 * 
 * here LibraryReducer is assigned to a key called "libraries"
 * our application state will have a key called "libraries" with value of whatever returned from
 * this reducer.
 * 
 *                      SUMMARY OF WHAT IS GOING ON:
 * When our application first boots up, redux creates a new store with the createStore call in App.js 
 * using LibraryReducer.
 * The instant the store is created, LibrariesReducers is called one time. We get a piece of state 
 * called "libraries"  which is an array containing a list of objects.
 * Created store is passed to "Provider" as a prop. Where it is gonna sit for the rest of our 
 * application's life.
 * "Provider" is a react component that helps the communication between react and redux.
 * Then App component is rendered to the screen, which in turn renders the "LibraryList" component.
 * The instant LibraryList component is about to render, "connect" function triggers. It reaches to 
 * the "Provider" and gets the state which is contained within the store.
 * "connect" helper then calls mapStateToProps with that state info, then whatever object(s) get 
 * returned shows up as props to the LibraryList component.
 * 
 * Reducer produces some amount of state even though we did not dispatch an action.
 * When our redux application first boots up, it will automatically run all the reducers to create an 
 * initial state.
 * 
 * We always design our reducers assuming they will be called over and over at randoms points in time.
 * This is why the type system is very important.
 * Reducers only change data whenever they receive an action with a type that they case about.
 * 
 * */
export default combineReducers({
    libraries: LibraryReducer,
    selectedLibraryId: SelectionReducer
});